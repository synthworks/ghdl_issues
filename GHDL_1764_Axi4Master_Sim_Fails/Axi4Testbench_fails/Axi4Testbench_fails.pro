#  File Name:         testbench.pro
#  Revision:          STANDARD VERSION
#
#  Maintainer:        Jim Lewis      email:  jim@synthworks.com
#  Contributor(s):
#     Jim Lewis      jim@synthworks.com
#
#
#  Description:
#        Script to run one Axi4 test  
#
#  Developed for:
#        SynthWorks Design Inc.
#        VHDL Training Classes
#        11898 SW 128th Ave.  Tigard, Or  97223
#        http://www.SynthWorks.com
#
#  Revision History:
#    Date      Version    Description
#     1/2020   2021.05    Initial for GHDL debug
#
#
#  This file is part of OSVVM.
#  
#  Copyright (c) 2021 by SynthWorks Design Inc.  
#  
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#  
#      https://www.apache.org/licenses/LICENSE-2.0
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#  

library osvvm_axi4
# Same as released Axi4Master
analyze Axi4Master.vhd

library osvvm_TbAxi4
## TestCtrl_e has external names to BurstFifos
# analyze TestCtrl_e.vhd  
## TestCtrl_e_ghdl comments out the BurstFifos
analyze TestCtrl_e_ghdl.vhd
# TB with Axi4Responder VC
analyze TbAxi4.vhd
# TB with Axi4Memory VC
analyze TbAxi4Memory.vhd

include ../TestCases/TestCases.pro
