# GHDL ISSUE 1764.   Axi4Master simulation fails
This analysis includes executable code.  
The scripts require running OSVVM's build script environment.

[[_TOC_]]


## Running the OSVVM build script environment
Clone OSVVM and the ISSUE repository:
```
git clone https://gitlab.com/synthworks/ghdl_issues.git
```

You will also need to clone the OsvvmLibraries repository (updated with this issue):
```
git clone --recursive https://github.com/osvvm/OsvvmLibraries
```

Go to ghdl_issues/sim:
```
cd ghdl_issues/sim
```

Now run GHDL at your linux or msys2 prompt:
```
winpty rlwrap tclsh
source ../../OsvvmLibraries/Scripts/StartUp.tcl
```

Compile the OsvvmLibraries
```
build ../../OsvvmLibraries
```


## Run the Test Cases
There are two test cases.   The first fails, the second works
as implied by their directory names.  

```
build ../GHDL_1764_Axi4Master_Sim_Fails/Axi4Testbench_fails
build ../GHDL_1764_Axi4Master_Sim_Fails/Axi4Testbench_works
```

Test output will be in the directory:   
`sim/logs/GHDL-<revision>`  
 
 
The only difference in the tests is the Axi4Master.vhd
and Axi4Master_ghdl.vhd. 
These files are in the above directories.
The differences are minor.
They are limited to the process WriteAddressHandler.
Specifically I commented out some code and put in 
temporary values.   

What is odd is that ReadAddressHandler is
almost identical and it works fine.  

So I could not find any pattern to what 
worked and why.  I only know what does and
does not work.  Hope this helps.  

## Bonus Test Case:  External Names to Shared Variables
OSVVM creates BurstFIFOs using shared variables.
The test sequencer accesses the FIFOs using 
external names.  It would help alot to have this
feature working.   

Run the test case using:

```
build ../GHDL_1764_Axi4Master_Sim_Fails/Axi4Testbench_bursting_external_names
```

OTOH, I have an experimental feature that may
replace the need for this, but it is probably 
6 months out.




