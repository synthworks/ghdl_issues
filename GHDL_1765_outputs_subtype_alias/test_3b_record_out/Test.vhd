library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TestPkg.all ;

entity test is
  port(
    input  : in  ARecType ;
    output : out ARecType 
  );
end entity;

architecture rtl of test is
  alias A is Output.A ; 

begin
  Output.A <= (A'range => '0') ;
  
  process 
  begin 
    wait on input ;  -- Suppress first run
    report "input.A = " & to_hstring(input.A) ; 
  end process ; 
  
end architecture;