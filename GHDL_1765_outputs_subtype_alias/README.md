# Driving aliases to unconstrained objects
This analysis includes executable code.  
The scripts require running OSVVM's build script environment.

[[_TOC_]]


## Running the OSVVM build script environment
Clone OSVVM and the ISSUE repository:
```
git clone https://gitlab.com/synthworks/ghdl_issues.git
```

You will also need to clone the OsvvmLibraries repository (same as for 1764):
```
git clone --recursive https://github.com/osvvm/OsvvmLibraries
```

Go to ghdl_issues/sim:
```
cd ghdl_issues/sim
```

Now run GHDL at your linux or msys2 prompt:
```
winpty rlwrap tclsh
source ../../OsvvmLibraries/Scripts/StartUp.tcl
```


## Run the Test Cases
There are five test cases.   Test cases 3a, 3c, and 3e all
fail.  If the constraints were not available, then 
3b and 3d should also fail, but they do not.

```
build ../GHDL_1765_outputs_subtype_alias/test_3a_record_alias_out
build ../GHDL_1765_outputs_subtype_alias/test_3b_record_out
build ../GHDL_1765_outputs_subtype_alias/test_3c_array_alias_out
build ../GHDL_1765_outputs_subtype_alias/test_3d_array_out
build ../GHDL_1765_outputs_subtype_alias/test_3e_record_alias_drive
```

3c also shows that if the alias is constrained
as shown below, it will also work.  However, since
the subtype information is available this should not be
necessary.

```vhdl
alias A is Output ; -- does not work
--  alias A : unsigned(output'range) is Output ;  -- Works
--  alias A : output'subtype is Output ;  -- Works
```

Test output will be in the directory:   
`sim/logs/GHDL-<revision>`  
