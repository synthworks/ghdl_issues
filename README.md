# Test Cases


## GHDL Issue #1764: Axi4Master_Sim_Fails

   See GHDL_1764_Axi4Master_Sim_Fails/README.md

## GHDL Issue #1765: Alias to unconstrained Output ports

   See GHDL_1765_outputs_subtype_alias/README.md